LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := main

SDL_PATH := ../SDL

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include \
	$(LOCAL_PATH)/../ffmpeg/include \
	$(LOCAL_PATH)/../libtest
	
# Add your application source files here...
LOCAL_SRC_FILES := $(SDL_PATH)/src/main/android/SDL_android_main.c \
	audio.cpp jniMplayer.c            
  
LOCAL_SHARED_LIBRARIES := SDL2 \
	  test avutil avcodec swresample avformat avresample postproc swscale  avfilter 
 #LOCAL_LDLIBS := test avutil avcodec swresample avformat avresample postproc swscale  avfilter
LOCAL_LDLIBS :=$(LOCAL_PATH)/../ffmpeg/lib/libswresample.so
LOCAL_LDLIBS += -lGLESv1_CM -lGLESv2 -llog
	
#LOCAL_ALLOW_UNDEFINED_SYMBOLS := true
LOCAL_CFLAGS := -D__STDC_CONSTANT_MACROS -std=c99
include $(BUILD_SHARED_LIBRARY)
