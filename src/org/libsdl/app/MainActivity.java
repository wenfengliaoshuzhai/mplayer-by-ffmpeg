package org.libsdl.app;

import cn.kapple.mplayer.Mplayer;
import android.hardware.Sensor;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {
	Button btopen,btclose,btnew;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		btopen=(Button)findViewById(R.id.button1);
		btclose=(Button)findViewById(R.id.button2);
		btnew=(Button)findViewById(R.id.button3);
		btopen.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Mplayer.open("mms://alive.rbc.cn/am774");
			}
			
		});
		//Mplayer.open("mmsh://alive.rbc.cn/am774");
		btclose.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Mplayer.stop();
			}
			
		});
		btnew.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Mplayer.open("mms://alive.rbc.cn/fm974");
			}
			
		});
		//mp.onCreate(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
